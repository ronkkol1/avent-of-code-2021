from typing import Callable


def fuel(start: int, goal: int):
    return abs(start - goal)


def fuel2(start: int, goal: int):
    step = abs(start - goal)
    costs = [i + 1 for i in range(step)]
    return sum(costs)


def find_cheapest_pos(start_positions: list[int], fuel_func: Callable[[int, int], int]):
    max_pos = max(start_positions)
    min_pos = min(start_positions)
    res = 0
    fuel_res = float('inf')
    for i in range(min_pos, max_pos + 1):
        fuel_i = sum(map(lambda a: fuel_func(a, i), start_positions))
        if fuel_i < fuel_res:
            res = i
            fuel_res = fuel_i
    return res, fuel_res


def main():
    # Change this line to try different inputs
    input = [3, 3, 3, 3, 6, 6, 6, 6]

    pos, fuel_amt = find_cheapest_pos(input, fuel)
    print(f"Cheapest horizontal pos is: {pos}, requiring {fuel_amt} fuel.")

    pos2, fuel_amt2 = find_cheapest_pos(input, fuel2)
    print(
        f"Cheapest horizontal pos with non-constant cost is: {pos2}, requiring {fuel_amt2} fuel.")


if __name__ == '__main__':
    main()
