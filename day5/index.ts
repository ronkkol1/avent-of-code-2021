import * as fs from "fs";

type coord = {
  x: number;
  y: number;
};

const dist = (a: coord, b: coord): number => {
  return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
};

const numOverlaps = (lines: Array<[coord, coord]>): number => {
  let overlaps = 0;
  for (let i = 0; i < 1000; i++) {
    for (let j = 0; j < 1000; j++) {
      const point: coord = {
        x: i,
        y: j,
      };
      let crossigns = 0;
      for (const line of lines) {
        if (
          Math.abs(
            dist(line[0], point) + dist(point, line[1]) - dist(line[0], line[1])
          ) < 0.0001
        ) {
          crossigns++;
        }
        if (crossigns > 1) break;
      }
      if (crossigns > 1) {
        overlaps++;
      }
    }
  }
  return overlaps;
};

const filename = process.argv[2];
console.log(filename);

const lines: Array<[coord, coord]> = [];

fs.readFileSync(filename, "utf-8")
  .split(/\r?\n/)
  .forEach((str) => {
    const coord1: coord = {
      x: Number(str.split(" ")[0].split(",")[0]),
      y: Number(str.split(" ")[0].split(",")[1]),
    };
    const coord2: coord = {
      x: Number(str.split(" ")[2].split(",")[0]),
      y: Number(str.split(" ")[2].split(",")[1]),
    };
    lines.push([coord1, coord2]);
  });

const res = numOverlaps(lines);
console.log(res);
