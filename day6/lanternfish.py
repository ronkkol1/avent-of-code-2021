class LanterfishPopulation:
    def __init__(self, fish: list[int]):
        self.day = 0
        self.counters: dict[int, int] = dict([(i, 0) for i in range(9)])
        for f in fish:
            self.counters[f] += 1

    def advance_day(self):
        new_counters: dict[int, int] = dict([(i, 0) for i in range(9)])
        for c in self.counters.keys():
            if c == 0:
                new_counters[6] += self.counters[c]
                new_counters[8] += self.counters[c]
            else:
                new_counters[c-1] += self.counters[c]
        self.counters = new_counters
        self.day += 1

    def size(self):
        size = 0
        for c in self.counters.keys():
            size += self.counters[c]
        return size


def main():
    # Change this line to try different inputs
    input = [3, 4, 3, 1, 2]

    pop = LanterfishPopulation(input)

    for i in range(256):
        pop.advance_day()
    print(pop.size())


if __name__ == "__main__":
    main()
