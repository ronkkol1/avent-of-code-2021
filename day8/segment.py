def detect_easy(digit: str):
    if (len(digit) == 2 or len(digit) == 3 or len(digit) == 4 or len(digit) == 7):
        return 1
    return 0


def decode_patterns(inputs: list[str]):
    # Init decodings
    dictionary = [set(('a', 'b', 'c', 'd', 'e', 'f', 'g')) for i in range(10)]

    # Find unique length patterns
    dictionary[1] = set(next(x for x in inputs if len(x) == 2))
    dictionary[7] = set(next(x for x in inputs if len(x) == 3))
    dictionary[4] = set(next(x for x in inputs if len(x) == 4))
    dictionary[8] = set(next(x for x in inputs if len(x) == 7))

    # Split remaining patterns by length
    fives = [set(x) for x in inputs if len(x) == 5]
    sixes = [set(x) for x in inputs if len(x) == 6]

    # Find patterns with set operations
    dictionary[9] = next(x for x in sixes if dictionary[4].issubset(x))
    dictionary[3] = next(x for x in fives if dictionary[1].issubset(x))
    dictionary[5] = next(
        x for x in fives if dictionary[7].union(x) == dictionary[9])
    dictionary[2] = next(x for x in fives if x !=
                         dictionary[5] and x != dictionary[3])
    dictionary[6] = next(
        x for x in sixes if dictionary[5].issubset(x) and x != dictionary[9])
    dictionary[0] = next(x for x in sixes if x !=
                         dictionary[9] and x != dictionary[6])

    return dictionary


def get_output_number(outputs: list[list[str]], dictionary: list[set[str]]):
    ints = list(map(lambda x: dictionary.index(set(x)), outputs))
    strs = [str(i) for i in ints]
    return int("".join(strs))


def main():
    f = open("input.txt")
    lines = f.readlines()
    f.close()
    outputs = list(map(lambda x: x.split("|")[1].split(), lines))
    inputs = list(map(lambda x: x.split("|")[0].split(), lines))

    output_digits = [output for sublist in outputs for output in sublist]
    nof_easy = sum(map(lambda x: detect_easy(x), output_digits))
    print(f"Outputs contain {nof_easy} unique length patterns.")

    total = 0
    for i in range(len(inputs)):
        total += get_output_number(outputs[i], decode_patterns(inputs[i]))
    print(f"Total sum: {total}")


if __name__ == "__main__":
    main()
